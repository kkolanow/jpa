package priv.konrad.jpa.model;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by konrad on 15.11.15.
 */
@Entity
@Table(name = "TodoLbok")
@Data
public class TodoLombok {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String summary;
    private String description;


}