package priv.konrad.jpa.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by konrad on 15.11.15.
 */
@Entity
@Data
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private double salary;
    private String jobDescr;


}
