package priv.konrad.jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by konrad on 15.11.15.
 */
@Data
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String firstName;
    private String lastName;

    @ManyToOne
    private Family family;

    @Transient
    private String filedNotPersisted;

    @OneToMany
    private List<Job> jobs = new ArrayList<Job>();

}
